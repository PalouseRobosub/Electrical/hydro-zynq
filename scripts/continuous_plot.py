import data_receiver
import argparse
import socket
import matplotlib.animation as anim
import matplotlib.pyplot as plt
import struct
from datetime import datetime

# script to continuosly graph hydrophone data
# expected to be used for raw sample data
# run rebroadcaster.py on the sub to send the sample data to the device that
# this script is running on

def make_zero_packet(packet_number):
    raw_data = []

    raw_data.append(struct.pack('<i', packet_number))
    raw_data.append(struct.pack('<hhhh', 0, 0, 0, 0))
    raw_data.append(struct.pack('<hhhh', 0, 0, 0, 0))

    return "".join(raw_data)

# function to be called repeatedly to graph the data
def update_graph(i, whole_data, channels, label_list, hostname, start_time):

    whole_data = []
    received_all_data = False
    started = False

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind((hostname, 3001))

    while not received_all_data:
        data = sock.recv(8 * 3000 + 4)

        packet = data_receiver.Packet(data)

        if packet.number == 0:
            if started:
                received_all_data = True
                for i, packet in enumerate(whole_data):
                    packet._parse()
                started = False
                sock.close()
            else:
                started = True
        if started:    
            whole_data.append(packet)

    np_array = data_receiver.to_numpy(whole_data)

    # clear graph and plot updated array
    data_plot.clear()
    for channel in channels:
        data_plot.plot(np_array[:, 0], np_array[:, channel+1])
        data_plot.grid(True)
        data_plot.legend(label_list)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--hostname', type=str, default='192.168.0.2', help='Specifies the hostname to bind to')
    args = parser.parse_args()

    # list of labels to use for graph
    label_list = ["Ch0", "Ch1", "Ch2", "Ch3"]

    channels = [0,1,2,3]

    whole_data = []

    fig = plt.figure()
    data_plot = fig.add_subplot(1, 1, 1)
    data_plot.grid(True)

    start_time = datetime.now()

    # calls update_graph repeatedly
    animator = anim.FuncAnimation(fig, update_graph, fargs=(whole_data, channels, label_list, args.hostname, start_time), interval=100)

    plt.xlabel('Time (s)')
    plt.show()
