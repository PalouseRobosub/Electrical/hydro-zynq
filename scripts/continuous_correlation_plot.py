import correlation_receiver
import argparse
import socket
import matplotlib.animation as anim
import matplotlib.pyplot as plt
import numpy as np

# script to continuously graph hydrophone correlation data

def update_graph(i, correlations, label_list, hostname):
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind((hostname, 3003))
    
    whole_data = []
    started = False
    received_all_data = False

    while not received_all_data:
        data = sock.recv(16 * 300 + 4)
        packet = correlation_receiver.Packet(data)

        if packet.number is 0:
            if started:
                received_all_data = True
                for i, packet in enumerate(whole_data):
                    packet._parse()
                started = False
                sock.close()
            else:
                started = True
        if started:
            whole_data.append(packet)

    np_array = correlation_receiver.to_numpy(whole_data)

    corr_plot.clear()
    for correlation in correlations:
        max_index = np.argmax(np_array[:, correlation+1])
        max_val = np_array[max_index, correlation+1]
        max_index = np_array[max_index, 0]

        corr_plot.plot(np_array[:, 0], np_array[:, correlation+1])
        corr_plot.scatter([max_index], [max_val], s=100, marker='x',
                                                        color='red')

    corr_plot.legend(label_list)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--hostname', type=str, default='192.168.0.2', help='specifies the hostname to bind to')
    args = parser.parse_args()

    label_list = ["Ch1-0", "Ch2-0", "Ch3-0"]

    correlations = [0,1,2]

    fig = plt.figure()
    corr_plot = fig.add_subplot(1, 1, 1)
    corr_plot.grid(True)

    animator = anim.FuncAnimation(fig, update_graph, fargs=(correlations, 
                label_list, args.hostname), interval=100)

    plt.show()
