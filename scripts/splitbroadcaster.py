import socket
import struct
import argparse

# script to broadcast received data to multiple alternate destinations
# useful if testing multiple receivers simultaneously

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('dstname1', type=str, help='Specifies the first output host to retransmit to in the form address:port')
    parser.add_argument('dstname2', type=str, help='Specifies the second output host to retransmit to in the form address:port')
    parser.add_argument('--hostname', type=str, default='192.168.0.2', help='Specifies the hostname to bind to')
    args = parser.parse_args()

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind((args.hostname, 3001))

    dst1 = args.dstname1.split(":")
    rebroadcast_socket1 = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    rebroadcast_socket1.connect((dst1[0], int(dst1[1])))

    dst2 = args.dstname2.split(":")
    rebroadcast_socket2 = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    rebroadcast_socket2.connect((dst2[0], int(dst2[1])))


    while True:
        data = sock.recv(8 * 3000 + 4)
        rebroadcast_socket1.send(data)
        rebroadcast_socket2.send(data)

